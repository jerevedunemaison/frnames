#encoding: utf-8
require 'minitest/autorun'
require_relative '../lib/frnames'

class FrNamesTest < Minitest::Test

  def test_main
    assert_equal("Isle-Adam", "L'ISLE-ADAM".frnamesup)
    assert_equal("Poggio-d'Oletta", "POGGIO-D'OLETTA".frnamesup)
    assert_equal("Île-Rousse", "L'ÎLE-ROUSSE".frnamesup)
    assert_equal("Crevans-et-la-Chapelle-les-Granges", "CREVANS-ET-LA-CHAPELLE-LES-GRANGES".frnamesup)
  end

end
