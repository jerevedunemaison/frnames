#encoding: utf-8

module StringHelper

  # == Returns
  # Good and valid french town name
  def frnamesup
    town = self
    #remove non chars
    town.gsub!(/[^[[:alpha:]]']+/, '-')
    #remove useless things
    town.strip!
    #capitalize
    town = town.gsub(/\w+/, &:capitalize)
    #EXCEPTION LE MANS
    return "Le Mans" if town == "Le-Mans"
    #Remove preposition
    town.gsub!(/\A(Le|La|Les|L\'|De|Des|D\')-*/, '')
    #replace the A, En, ...
    town.gsub!('Le-', 'le-')
    town.gsub!('La-', 'la-')
    town.gsub!('Les-', 'les-')
    town.gsub!('L\'', 'l\'')
    town.gsub!('De-', 'de-')
    town.gsub!('Des-', 'des-')
    town.gsub!('D\'', 'd\'')
    town.gsub!('En-', 'en-')
    town.gsub!('Sous-', 'sous-')
    town.gsub!('Sur-', 'sur-')
    town.gsub!('Lès-', 'lès-')
    town.gsub!('Près-', 'près-')
    town.gsub!('Aux-', 'aux-')
    town.gsub!('Au-', 'au-')
    town.gsub!('Devant-', 'devant-')
    town.gsub!('Et-', 'et-')
    #return final value
    return town
  end

end

class String
  prepend StringHelper
end
