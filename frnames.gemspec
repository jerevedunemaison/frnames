require_relative "lib/frnames/version"

Gem::Specification.new do |s|
  s.name        = 'frnames'
  s.version     = FrNames::VERSION
  s.date        = '2014-08-25'
  s.summary     = "A gem to create good names ini"
  s.description = "A gem to create good names for the fkngs France's towns"
  s.authors     = [
            	  "poulet_a"
		  ]
  s.email       = "poulet_a@epitech.eu",
  s.files       = [
               	  "lib/frnames.rb",
              	  "lib/frnames/caser.rb",
		  ]
  s.homepage    = "https://gitlab.com/jerevedunemaison/frnames"
  s.license     = "GNU/GPLv3"
end
